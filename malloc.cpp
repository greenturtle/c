#include <stdio.h>
#include <stdlib.h>
#include <iostream>

int main(){
	// Reminding the basic
	int value = 10;
	int* pValue;

	pValue = &value;

	printf("Value: %d",value);
	printf("\nAddress: %x",pValue);
	printf("\nValue by reference: %d\n",*pValue);

	int *pointer;
	pointer = (int*) malloc(sizeof(int));
	*pointer = 22;
	printf("%d\n",*pointer);

	// Strings
	std::string name = std::string("JoJo") + " Bizzare Adventures";
	bool contains = name.find("Adv") != std::string::npos;
	std::cout << name << " "<< contains<<std::endl;
	// Vectors
	int x;
	int op=1;
	int numbers[0];


	while (op){
		printf(
		"\nDo you wish save a number?"
		"\n0: No, thanks"
		"\n1: Yes, please"
		"Type: "
		);
		scanf("%d",&op);
		
	}
	
	
	
	return 0;
}
